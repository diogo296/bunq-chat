<?php

require_once('../config.php');
require_once(MODELS_PATH . "model.php");

class User extends Model {
  public $name;

  function __construct($attr=[]) {
    // Creates a name if not set
    if (!isset($attr['name'])) {
      $attr['name'] = $this->create_name();
    }

    parent::__construct($attr, [
      "name" => "VARCHAR(15) NOT NULL"
    ]);

    $this->name = $attr["name"];
  }

  // Create a random name (animal<random number>)
  private function create_name() {
    $names = ["octopus", "bunny", "kitten", "spider", "hamster",
      "crocodile", "shark", "ant", "ape", "bison",
      "cheetah", "lion", "chicken", "crow", "dinosaur",
      "duck", "falcon", "goat", "hawk", "leopard"];
    return $names[rand(0, count($names)-1)] . rand(0,1000);
  }
}

?>
