<?php

require_once('../config.php');
require_once(MODELS_PATH . 'message.php');

/* Generates messages to be inserted in the DB
 * MAX_USER_ID: max user id in the DB
 * NUM_MESSAGES: number of messages to be generated
*/

CONST MAX_USER_ID  = 50;
CONST NUM_MESSAGES = 100;

for ($i = 1; $i <= NUM_MESSAGES; $i++) {
  $data = [];

  $data['uid_from'] = 0;
  $data['uid_to']   = 0;
  $data['text']     = "Message " . $i;

  while ($data['uid_from'] == $data['uid_to']) {
    $data['uid_from'] = rand(1, MAX_USER_ID);
    $data['uid_to']   = rand(1, MAX_USER_ID);
  }

  $msg = new Message($data);
  $msg->create();
  echo "Message " . $i . " created!\n";
}

?>
