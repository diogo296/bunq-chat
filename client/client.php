<?php

require_once('../config.php');

// Creates one thread to send messages and another to retrieve messages
class Client {
  private $id;
  const SERVER_URL = APP_URL . "server/process_request.php";  // URL to send requests

  function __construct($id) {
    $this->id = $id;
  }

  // Sends messages inserted from the keyboard
  public function send_messages() {
    $send_thread = new SendMessages($this->id, self::SERVER_URL);
    $send_thread->start();
  }

  // Retrieve messages sent to user
  public function get_messages() {
    $get_thread = new GetMessages($this->id, self::SERVER_URL);
    $get_thread->start();
  }
}

abstract class Request extends Thread {
  const SLEEP_TIME = 5;  // In seconds

  protected $user_id;
  protected $url;
  protected $data;

  function __construct($user_id, $url) {
    $this->user_id = $user_id;
    $this->url     = $url;
  }

  // Send a HTTP request using curl and enconding the data to JSON
  protected function post_curl($data) {
    $curl = curl_init($this->url);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($curl);
    curl_close($curl);

    return $result;
  }
}

// Reads the user_to ID and message from the keyboard and sends to the
// server
class SendMessages extends Request {
  public function run() {
    $data = ['send_message' => []];
    $data['send_message']['uid_from'] = $this->user_id;

    // Sends messages indefinitely
    while (true) {
      echo "Writing message...\n";

      $data['send_message']['uid_to'] = intval(readLine("User to ID: "));
      $data['send_message']['text']   = readLine("Message: ");

      echo parent::post_curl($data);
    }
  }
}

// Periodically polls for new messages for user
class GetMessages extends Request {
  public function run() {
    $data = ['get_messages' => []];
    $data['get_messages']['uid_to'] = $this->user_id;

    // Request messages indefinitely
    while (true) {
      echo "Messages to User " . $this->user_id . "\n";
      echo parent::post_curl($data);

      echo "Refresh in " . self::SLEEP_TIME . "s...\n";
      sleep(self::SLEEP_TIME);
    }
  }
}

?>
