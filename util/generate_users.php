<?php

require_once('../config.php');
require_once(MODELS_PATH . 'user.php');

/* Generates users to be inserted in the DB
 * NUM_USERS: number of users to be generated
*/

CONST NUM_USERS = 50;

for ($i = 1; $i <= NUM_USERS; $i++) {
  $user = new User();
  $user->create();
  echo "User " . $i . " created!\n";
}

?>
