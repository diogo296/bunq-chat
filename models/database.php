<?php

require_once('../config.php');

// Singleton class
class Database {
  private static $connection;

  private function __construct() {
  }

  private function __clone() {
  }

  public static function connect() {
    // One connection through whole application
    if (static::$connection == null) {
      try {
        static::$connection = new SQLite3(DB_PATH);
      }
      catch(Exception $e) {
        die($e->getMessage());
      }
    }

    return static::$connection;
  }

  public static function disconnect()
  {
    static::$connection->close();
    static::$connection = null;
  }
}

?>
