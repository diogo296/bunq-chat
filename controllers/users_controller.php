<?php

require_once('../config.php');
require_once(MODELS_PATH . "user.php");

class UsersController {
  // Uses model to create new user
  public function create($params) {
    $user = new User($params);
    $user->create();
  }
}

?>
