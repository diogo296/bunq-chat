<?php

require_once('../config.php');
require_once(MODELS_PATH . 'database.php');

abstract class Model {
  protected $attr;  // Will store all attributes from child class

  function __construct($attr, $attr_types) {
    $this->create_table($attr_types);
    $this->set_attr($attr);
  }

  // Returns table name from child class model
  private function table_name() {
    return get_called_class() . 's';
  }

  private function set_attr($attr) {
    $this->attr = $attr;
  }

  /* Creates a table for the model
   * $fields: table column names and types as array */
  protected function create_table($fields) {
    $sql = "CREATE TABLE IF NOT EXISTS " . self::table_name() . " (";
    $sql .= "id INTEGER PRIMARY KEY AUTOINCREMENT";

    foreach($fields as $field => $type) {
      $sql .= ", " . $field . " " . $type;
    }
    $sql .= ");";

    $db = Database::connect();
    $db->exec($sql);
    Database::disconnect();
  }

  // Inserts a row in the DB with the child model attributes
  public function create() {
    $names  = " (";
    $values = "(";

    // Join names and values in a string
    foreach ($this->attr as $name => $value) {
      $names  .= $name . ",";
      if (is_string($value)) {
        $values .= "'" . $value . "',";
      }
      else {
        $values .= $value . ",";
      }
    }

    // Replace last ',' for a ')'
    $names[strlen($names)-1] = ")";
    $values[strlen($values)-1] = ")";

    $sql = "INSERT INTO " . self::table_name() . $names . " VALUES " . $values . ";";

    $db = Database::connect();
    $db->exec($sql);
    Database::disconnect();
  }

  // Convert SQL result array to child model type
  private static function data_to_model($data) {
    $objs = [];
    $class_name = rtrim(self::table_name(), "s");  // Model class name is the table name in singular

    foreach ($data as $row) {
      $objs[] = new $class_name($row);
    }

    return $objs;
  }

  /* Do a simple query in the DB
   * Query format: 'SELECT * FROM <table_name> [WHERE attr1 = value1 AND attr2 = value2]
   * $where: array, each element as a column to query
   * */
  public static function find($where=[]) {
    $table_name = self::table_name();
    $sql = "SELECT * FROM " . $table_name;

      // Join $where array to a string
    if (!empty($where)) {
      $sql .= " WHERE";

      foreach ($where as $field => $value) {
        if (is_string($value)) {
          $value = "'" . $value . "'";
        }
        $sql .= " " . $field . " = " . $value . " AND";
      }
    }

    $sql = rtrim($sql, " AND");  // Remove last AND in query string
    $sql .= ";";

    $db = Database::connect();
    $query_result = $db->query($sql);

    // Store query results in a array
    $data = [];
    while ($row = $query_result->fetchArray(SQLITE3_ASSOC)) {
      unset($row["id"]);
      $data[] = $row;
    }

    Database::disconnect();
    return self::data_to_model($data);
  }

  // Return all columns from a table
  public static function find_all() {
    return self::find();
  }
}

?>
