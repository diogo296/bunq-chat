<?php

/* An example of client refreshing to pool for new messages
 * The user inserts his ID in the terminal and waits for new messages
 * It must be an user already in the database
* */

require_once('../config.php');
require_once(CLIENT_PATH . 'client.php');

$user_id = intval(readLine("Insert your ID: "));
$client = new Client($user_id);
$client->get_messages();

?>
