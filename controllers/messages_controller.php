<?php

require_once('../config.php');
require_once(MODELS_PATH . "message.php");
require_once(MODELS_PATH . "user.php");

class MessagesController {
  // Create a new message only if the user already exists
  public function create($params) {
    if (!empty(User::find(['id' => $params['uid_to']]))) {
      $msg = new Message($params);
      $msg->create();
      return true;
    }
    else {
      return false;
    }
  }

  // Return messages found in DB
  public function show($params) {
    return Message::find($params);
  }
}

?>
