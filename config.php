<?php

/* Sets project paths to constants
 * You may need to set APP_URL and DB_PATH */

// Application root URL
defined("APP_URL")
or define("APP_URL", "localhost/bunq-chat/");

defined("ROOT_PATH")
or define("ROOT_PATH", realpath(dirname(__FILE__)) . '/');

// Database file location
defined("DB_PATH")
or define("DB_PATH", ROOT_PATH . 'data/bunq_chat.sqlite');

defined("MODELS_PATH")
or define("MODELS_PATH", ROOT_PATH . 'models/');

defined("CONTROLLERS_PATH")
or define("CONTROLLERS_PATH", ROOT_PATH . 'controllers/');

defined("CLIENT_PATH")
or define("CLIENT_PATH", ROOT_PATH . 'client/');

?>
