<?php

/* An example of client sending messages
 * The user inserts his ID in the terminal and then the user ID to and the message
 * It must be an user already in the database
* */

require_once('../config.php');
require_once(CLIENT_PATH . 'client.php');

$user_id = intval(readLine("Insert your ID: "));
$client = new Client($user_id);
$client->send_messages();

?>
