<?php

require_once('../config.php');
require_once(MODELS_PATH . "model.php");

class Message extends Model {
  public $uid_from, $uid_to;
  public $text;
  public $date;

  function __construct($attr) {
    if (!isset($attr["date"])) {
      $attr["date"] = date('Y-m-d H:i:s');  // Current date
    }

    parent::__construct($attr, [
      "uid_from" => "INTEGER REFERENCES users(id)",
      "uid_to"   => "INTEGER REFERENCES users(id)",
      "text"     => "VARCHAR(255) NOT NULL",
      "date"     => "TIMESTAMP    NOT NULL"
    ]);

    $this->uid_to   = $attr["uid_to"];
    $this->uid_from = $attr["uid_from"];
    $this->text     = $attr["text"];
    $this->date     = $attr["date"];
  }

}

?>
