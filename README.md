# BunqChat #

Simple PHP chat backend. Client requests simulated by threads sending HTTP requests to the server.

### Requirements ###

* PHP7+
* A web service (like Apache)
* [pthreads](https://github.com/krakjoe/pthreads)

### How to set up ###

1. Set the database path and the application URL root path in the file *config.php*.
2. Make sure the folder *data* and the database file are writtable.
3. Make sure there are clients in the DB. You can insert some using the script *util/generate_users.php*.

### Testing the chat ###

After setting everything up and running the application in the web server, open a terminal and do the following:

1. Run the script *client/client_receive.php*, then insert the user ID in the terminal and wait for the script retrieve new messages.
2. In another terminal, run *client/client_send.php*. Insert the user ID that will send messages and then insert, how many times you want, the user ID that will receive the message and the message itself.

### Notes ###

* The application assumes the user is already in the database, so users that aren't already in the DB can't send or receive messages.
* The folder *util* contains some useful code to generate data and create the DB tables.