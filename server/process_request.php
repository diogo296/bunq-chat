<?php

require_once('../config.php');
require_once(CONTROLLERS_PATH . 'messages_controller.php');

// Read and decode data sent to server
$data = file_get_contents("php://input");
$data = json_decode($data, true);

$msg_controller = new MessagesController();

// Create a new message and save to DB
if (isset($data['send_message'])) {
  $success = $msg_controller->create($data['send_message']);

  if ($success) {
    echo "Message sent!\n";
  }
  else {
    echo "User doesn't exists!\n";
  }
}

// Retrieve messages from DB
if (isset($data['get_messages'])) {
  $msgs = $msg_controller->show($data['get_messages']);

  // Print all messages to user
  if (empty($msgs)) {
    echo "There's no messages for this user.\n";
  }
  else {
    echo "FROM | MESSAGE | DATE\n";
    foreach ($msgs as $msg) {
      echo $msg->uid_from . " | " . $msg->text . " | " . $msg->date . "\n";
    }
  }
}

?>
